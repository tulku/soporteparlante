// todas las medidas en mm
alto_triangulo = 215;
lado_triangulo = 30;

ancho_base = 90;
profundidad_base = 50;
alto_base = 5;

alto_enganche = 10;
enganche = 5;

module gancho() {
	gancho_prof1 = 11;
	gancho_prof2 = 3;
	gancho_h1 = 15;
	gancho_h2 = 8;
	luz = 2;
	translate([(gancho_prof2+gancho_prof1)/2, luz/2, 0]) rotate([90, 180, 0]) {
	linear_extrude(height = luz) polygon(points = [
	[0, 0], [gancho_prof1, 0],
	[gancho_prof1, gancho_h1], [gancho_prof2, gancho_h1],
	[gancho_prof2, gancho_h2], [0, gancho_h2]
	], paths=[[0,1,2,3,4,5,0]]);
	}
}

//Draw a prism based on a
//right angled triangle
//l - alto del prisma
//w - ancho del triangulo
//h - alto del triangulo
module prism_iso(l, w, h) {
	translate([0, -h/2, 0]) {
	linear_extrude(height = l) polygon(points = [
	[-w/2, 0], [w/2, 0], [0, h]
	], paths=[[0,1,2,0]]);
	}
}

module mitad_abajo() {
	translate([0, 0, alto_triangulo/2]) {
	prism_iso(alto_enganche, lado_triangulo-enganche, lado_triangulo-enganche);
	}
	prism_iso(alto_triangulo/2, lado_triangulo, lado_triangulo);
}

module poste() {
	translate([0, 0, -alto_triangulo/2]) {
	difference() {
	prism_iso(alto_triangulo, lado_triangulo, lado_triangulo);
	mitad_abajo();
	#translate([5.0, -10.5, 210.5]) {
		rotate([0, 0, 26]) {
			gancho();
		}
	}
   	#translate([-5.0, -10.5, 210.5]) {
		rotate([0, 0, 180-26]) {
			gancho();
		}
	}

	}
	}
}

//// poste, parte de abajo
//translate([0, -lado_triangulo/4, alto_base-1]) {
//	difference() {
//	mitad_abajo();
//	#translate([5.0, -10.5, 100.5]) {
//		rotate([0, 0, 26]) {
//		gancho();
//		}
//	}
//   translate([-5.0, -10.5, 100.5]) {
//		rotate([0, 0, 180-26]) {
//		gancho();
//		}
//	}
//
//	}
//}
//
//
//// Base
//color("red") {
//	prism_iso(alto_base, ancho_base, profundidad_base);
//}



translate([-50, 0, 25])
{
rotate([-90, 0, 0])
{
// mitad superior
color("blue") {
	translate([50, 10, alto_triangulo/2]) {
		rotate([-180, 0, 0]){
			poste();			
		}
	}
}
}
}
//gancho();